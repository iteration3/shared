# Meeting Scheduler System

System requirements for a fictitious meeting scheduler system

## High-level requirements

The following text describes high level 

### [requirement id=REQ0001]

The system shall support creating invitations to participants.

### [requirement id=REQ0002 quality=QR0003]

The system shall support creating a meeting event in a personal calendar.

### [requirement id=REQ0003]

The system shall support event labeling

### [requirement id=REQ0004]

The system shall allow scheduling of resources

### [requirement id=REQ0004]

The system shall support booking

### [requirement id=REQ0005]

The system shall allow viewing booking

