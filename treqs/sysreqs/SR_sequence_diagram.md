# Hello there

```puml
@startuml Sequence diagram for Meeting Scheduler System
note right of administrator
/'[requirement id=REQ0005 quality=QR0003 test=TC0004,TC0006]'/
end note
participant administrator
participant initiator
participant participant
administrator ->system: Maintains user profile
activate administrator
activate system
participant -> system: Proposes meeting time and place
deactivate administrator
system-->meetinginitiator: Returns the available time slots

@enduml
```

